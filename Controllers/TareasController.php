<?php
require_once('Controllers/Conexion.php');
require_once('Models/Tareas.php');
require_once('Models/Categorias.php');

class TareasController{

    public function obtenerTareas(){
        $db= new Conexion();
        $result=$db->query("select * from tareas order by id desc");

        $arregloTareas=[];
        while($tarea=$db->fetch_array($result))
        {
            $oTarea= new Tareas();
            $oTarea->setDetalle($tarea['detalle']);
            $oTarea->setId($tarea['id']);
            $oTarea->setCategorias($this->obtenerCategorias($oTarea));
            $arregloTareas[]=$oTarea;
        }
        
        $db->close();
        return $arregloTareas;
    }

    public function obtenerCategorias(Tareas $oTarea)
    {
        $db= new Conexion();
        $result=$db->query("select categorias.* 
                            from categorias_tareas
                             join categorias on categorias_id=id
                             order by nombre");

        $arregloCategorias=[];
        while($categoria=$db->fetch_array($result))
        {
            $oCategoria= new Categorias();
            $oCategoria->setNombre($categoria['nombre']);
            $oCategoria->setId($categoria['id']);
            $arregloCategorias[]=$oCategoria;
        }
        
        $db->close();
        return $arregloCategorias;
    }

}