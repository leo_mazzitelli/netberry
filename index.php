<?php
require_once("Controllers/TareasController.php");

$oTareasController= new TareasController();
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <title>Netberry</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

<div class="page-header">
    <h1>Netberry <small>Test</small></h1>
</div>

<div class="container">
    <table class="table table-bordered">
    <thead>
        <tr>
            <td>Tarea</td>
            <td>Categoria</td>
            <td>Acciones</td>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($oTareasController->obtenerTareas() as $oTarea)
            {
                ?>
                <tr>
                    <td><?php echo $oTarea->getDetalle() ?></td>
                    <td>
                        <?php
                            foreach($oTarea->getCategorias() as $oCategoria)
                            {
                             echo '<span class="label label-warning">'.$oCategoria->getNombre().'</span>';   
                            }
                        ?>
                    </td>
                    <td><button class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button></td>
                </tr>
                <?php
            }
        ?>
    </tbody>
    </table>
</div>


</div>

</body>
