<?php

class Tareas
{
    /**
     * Id de la tarea.
     *
     * @var int
     */
    private $id;
    /**
     * Detalle de la Tarea.
     *
     * @var string
     */
    private $detalle;

    /**
     * Categorias de la tarea.
     *
     * @var array
     */
    private $categorias;

    /**
     * Get the value of Id de la tarea.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id de la tarea.
     *
     * @param int id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Detalle de la Tarea.
     *
     * @return string
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set the value of Detalle de la Tarea.
     *
     * @param string detalle
     *
     * @return self
     */
    public function setDetalle(String $detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get the value of Categorias de la tarea.
     *
     * @return array
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set the value of Categorias de la tarea.
     *
     * @param array categorias
     *
     * @return self
     */
    public function setCategorias(array $categorias)
    {
        $this->categorias = $categorias;

        return $this;
    }
}
