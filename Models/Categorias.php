<?php

class Categorias
{
    /**
     * Id de la categoría.
     *
     * @var int
     */
    private $id;

    /**
     * Nombre de la categoría.
     *
     * @var string
     */
    private $nombre;

    /**
     * Get the value of Id de la categoría.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id de la categoría.
     *
     * @param int id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Nombre de la categoría.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of Nombre de la categoría.
     *
     * @param string nombre
     *
     * @return self
     */
    public function setNombre(String $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }
}
